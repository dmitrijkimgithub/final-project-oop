# Final Project OOP
Author:Dmitriy Kim

Introduction to the Project:
----------------------------
The Furniture Management System project aims to facilitate the administration of a furniture items database through a 
console-based interface. Users can utilize this system to search, add, and delete items. Developed in Java, the system 
incorporates technologies like HikariCP for connection pooling, PostgreSQL as the database, and SLF4J for straightforward logging. It includes an admin panel that mandates authorization for performing administrative tasks.

PostgreSQL Database:
--------------------
Unfortunately it is only available locally, since I couldn't host it on Microsoft Azure. I provided SQL script for creating
table in Dbeaver. Please create database in pgAdmin4, and after creating find it in Dbeaver. After that run the SQL script.
However, you will need to change the URL, username and password in AdminAuthorization, DBConnectionManager, AdminAuthorizationTest
and PostgresDataFetchingTest.

Panel for Admin:
--------------------
Authentication is necessary to perform administrative actions in the Furniture Management System's admin panel. 
The designated admin username and password are "itpuPostgres/student."

Some info about the classes. What role do they play?
-------------------

AdminAuthorization:

The AdminAuthorization class manages user authorization for admin access. It establishes a connection to a PostgreSQL 
database and verifies whether the provided username and password correspond to any entries in the 'admin' table. 
If an authorized admin is identified, the method returns true; otherwise, it returns false. The class is designed to handle 
SQLExceptions and, in case of errors, prints the stack trace for diagnostic purposes.

ControlPanel:

The ControlPanel class serves as the user interface for the furniture management system, catering to both regular users
with limited privileges and administrators with elevated privileges. It collaborates with the SearchSystemFurniture,
AdminAuthorization, and FurnitureWarehouseDatabase to facilitate tasks such as searches, addition or removal of items, and overall 
collection management.

DBConnectionManager:

The DBConnectionManager class oversees the HikariCP connection pool for PostgreSQL. It offers a static method, getDataSource(), 
which retrieves the configured HikariDataSource. Connection specifics, including JDBC URL, username, and password, are 
established within the static block.

FurnitureWarehouseDatabase:

The FurnitureWarehouseDatabase class engages with the database to execute CRUD operations on furniture records. 
It employs HikariDataSource for managing database connections and incorporates methods to load furniture data, 
add new records, generate new IDs, delete records, and verify the existence of collections.

Main:

The Main class functions as the starting point for the furniture management application. It establishes a connection to 
the database using HikariDataSource, loads furniture data, and initializes essential components like SearchSystemFurniture,
AdminAuthorization, and ControlPanel. The application concludes gracefully by closing the connection pool.

Gitlab Link
------------------
https://gitlab.com/dmitrijkimgithub/final-project-oop.git