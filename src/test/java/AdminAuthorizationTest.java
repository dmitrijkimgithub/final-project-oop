import org.application.admin.AdminAuthorization;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AdminAuthorizationTest {

    @Test
    public void isAdminTest() {
        AdminAuthorization authorizationService = new AdminAuthorization();

        assertTrue(authorizationService.isAdminAuthorized("itpuPostgres", "student"));

        assertFalse(authorizationService.isAdminAuthorized("invalidAdmin", "invalidPassword"));
    }
}

