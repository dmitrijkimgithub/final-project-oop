import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PostgresDataFetchingTest {

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/Warehouse";
    private static final String USER = "postgres";
    private static final String PASSWORD = "200304";

    public static void main(String[] args) {
        List<FurnitureItem> furnitureItems = loadFurnitureItemsFromDatabase();

        for (FurnitureItem item : furnitureItems) {
            System.out.println(item);
        }
    }

    private static List<FurnitureItem> loadFurnitureItemsFromDatabase() {
        List<FurnitureItem> furnitureItems = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(DB_URL, USER, PASSWORD)) {
            String sql = "SELECT * FROM warehouse_table";
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql);
                 ResultSet resultSet = preparedStatement.executeQuery()) {

                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String collection = resultSet.getString("collection");
                    String name = resultSet.getString("name");
                    String description = resultSet.getString("description");
                    double price = resultSet.getDouble("price");
                    String color = resultSet.getString("color");
                    String material = resultSet.getString("material");

                    FurnitureItem item = new FurnitureItem(id, collection, name, description, price, color, material);
                    furnitureItems.add(item);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return furnitureItems;
    }

    private record FurnitureItem(int id, String collection, String name, String description, double price, String color,
                                 String material) {
        @Override
        public String toString() {
            return "FurnitureItem{" +
                    "id=" + id +
                    ", collection='" + collection + '\'' +
                    ", name='" + name + '\'' +
                    ", description='" + description + '\'' +
                    ", price=" + price +
                    ", color='" + color + '\'' +
                    ", material='" + material + '\'' +
                    '}';
        }
    }
}
