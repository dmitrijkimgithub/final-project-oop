import org.application.searches.SearchCriteria;
import org.application.elements.SpecFurniture;
import org.application.searches.SearchSystemFurniture;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SearchSystemTest {
    private SearchSystemFurniture<SpecFurniture> searchEngine;

    @BeforeEach
    public void setup() {
        List<SpecFurniture> furnitureList = new ArrayList<>();
        furnitureList.add(new SpecFurniture(1, "Contemporary", "Bed", "Easy to assemble and maintain.", 1143.0, "Black", "Plastic"));
        furnitureList.add(new SpecFurniture(2, "Modernist", "Desk", "Adds a touch of elegance to any room.", 903.0, "White", "Plastic"));

        searchEngine = new SearchSystemFurniture<>(furnitureList);
    }

    @Test
    public void testSearchById() {
        List<SpecFurniture> results = searchEngine.searchById(1);
        Assertions.assertEquals(1, results.size(), "Incorrect number of search results");
        Assertions.assertEquals("Bed", results.get(0).name, "Incorrect search result");
    }

    @Test
    public void testSearchByNameKeyword() {
        List<SpecFurniture> results = searchEngine.searchByNameKeyword("Desk");
        Assertions.assertEquals(1, results.size(), "Incorrect number of search results");
        Assertions.assertEquals("Desk", results.get(0).name, "Incorrect search result");
    }

    @Test
    public void testSearchByDescriptionKeyword() {
        List<SpecFurniture> results = searchEngine.searchByDescriptionKeyword("assemble");
        Assertions.assertEquals(1, results.size(), "Incorrect number of search results");
        Assertions.assertEquals("Bed", results.get(0).name, "Incorrect search result");
    }

    @Test
    public void testSearchByPriceRange() {
        List<SpecFurniture> results = searchEngine.searchByPriceRange(1000.0, 2000.0);
        Assertions.assertEquals(1, results.size(), "Incorrect number of search results");
    }

    @Test
    public void testSearchByColor() {
        List<SpecFurniture> results = searchEngine.searchByColor("White");
        Assertions.assertEquals(1, results.size(), "Incorrect number of search results");
    }

    @Test
    public void testSearchByMaterial() {
        List<SpecFurniture> results = searchEngine.searchByMaterial("Plastic");
        Assertions.assertEquals(2, results.size(), "Incorrect number of search results");
    }

    @Test
    public void testSearchByCriteria() {
        SearchCriteria<SpecFurniture> criteria = new SearchCriteria<>("Modernist", "Bed", 1000.0, 5000.0, "Black", "Glass");

        List<SpecFurniture> results = searchEngine.searchByCriteria(criteria);
        Assertions.assertEquals(0, results.size(), "Incorrect number of search results");
    }
}
