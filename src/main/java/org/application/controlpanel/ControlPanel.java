package org.application.controlpanel;

import org.application.admin.AdminAuthorization;
import org.application.db.FurnitureWarehouseDatabase;
import org.application.searches.SearchSystemFurniture;
import org.application.searches.SearchCriteria;
import org.application.elements.SpecFurniture;
import org.application.elements.Furnishings;
import org.application.interfaces.ControlPanelInterface;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;


public class ControlPanel implements ControlPanelInterface {
    private SearchSystemFurniture searchSystem;
    private final FurnitureWarehouseDatabase database;

    private boolean isAdmin = false;

    private final AdminAuthorization adminAuthorization;
    public ControlPanel(SearchSystemFurniture searchSystem, AdminAuthorization adminAuthorization, FurnitureWarehouseDatabase database) {
        this.searchSystem = searchSystem;
        this.adminAuthorization = adminAuthorization;
        this.database = database;
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Are you an admin or a user? Enter 'admin' or 'user': ");
        String userType = scanner.nextLine().toLowerCase();

        if ("admin".equals(userType)) {
            isAdmin = authorizeAdmin(scanner);
            if (isAdmin) {
                System.out.println(" ");
                System.out.println("Successful Authorization!");
                System.out.println(" ");
            } else {
                System.out.println("Authorization failed. Quiting the program.");
                return;
            }
        } else if (!"user".equals(userType)) {
            System.out.println("Invalid user type. Quiting the program.");
            return;
        }

        while (true) {
            displayMenu();
            int option;

            try {
                option = scanner.nextInt();
                scanner.nextLine();
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please enter a number.");
                scanner.nextLine();
                continue;
            }

            switch (option) {
                case 1 -> searchById(scanner);
                case 2 -> searchByKeyword(scanner, "collection");
                case 3 -> searchByKeyword(scanner, "name");
                case 4 -> searchByKeyword(scanner, "description");
                case 5 -> searchByPrice(scanner);
                case 6 -> searchByColor(scanner);
                case 7 -> searchByMaterial(scanner);
                case 8 -> searchByCriteria(scanner);

                // Only for admins
                case 9 -> {
                    if (isAdmin) {
                        addUserItem(scanner);
                        refreshSearchSystemData();
                    } else {
                        System.out.println("You do not have permission to modify.");
                    }
                }
                case 10 -> {
                    if (isAdmin) {
                        deleteUserItems(scanner);
                        refreshSearchSystemData();
                    } else {
                        System.out.println("You do not have permission to modify.");
                    }
                }
                case 11 -> {
                    if (isAdmin) {
                        addUserCollection(scanner);
                        refreshSearchSystemData();
                    } else {
                        System.out.println("You do not have permission to modify.");
                    }
                }

                case 0 -> {
                    System.out.println("Quiting the program...");
                    return;
                }
                default -> System.out.println("Invalid option. Please try again.");
            }

            System.out.println();
        }
    }

    private boolean authorizeAdmin(Scanner scanner) {
        int maxAttempts = 5;
        int attempts = 0;

        while (attempts < maxAttempts) {
            System.out.print("Enter admin username: ");
            String adminUsername = scanner.nextLine();

            System.out.print("Enter admin password: ");
            String adminPassword = scanner.nextLine();

            if (adminAuthorization.isAdminAuthorized(adminUsername, adminPassword)) {
                return true;
            } else {
                attempts++;
                System.out.println("Incorrect username or password. Please try again.");
                System.out.println("Attempts left: " + (maxAttempts - attempts));
            }
        }

        return false;
    }

    private void displayMenu() {
        System.out.println("1. Search by ID");
        System.out.println("2. Search by Collection");
        System.out.println("3. Search by Keyword of Name");
        System.out.println("4. Search by Keyword of Description");
        System.out.println("5. Search by Price");
        System.out.println("6. Search by Color");
        System.out.println("7. Search by Material");
        System.out.println("8. Search by Criteria (Multiple Choices)");

        if (isAdmin) {
            System.out.println("9. Add new item to the Furniture Warehouse database");
            System.out.println("10. Remove item from the Furniture Warehouse database");
            System.out.println("11. Add new collection to the Furniture Warehouse database");
        } else {
            System.out.println("9. Add new item to DB - requires Authorization");
            System.out.println("10. Remove item from DB - requires Authorization");
            System.out.println("11. Add new collection to DB - requires Authorization");
        }

        System.out.println(" ");
        System.out.println("0. Press 0 to Quit");
        System.out.println(" ");
        System.out.print("Enter your option: ");
    }

    private void searchById(Scanner scanner) {
        System.out.print("Enter the furniture ID: ");
        int id = scanner.nextInt();

        List<Furnishings> results = searchSystem.searchById(id);
        displaySearchResults(results);
    }

    private void searchByKeyword(Scanner scanner, String field) {
        System.out.print("Enter the keyword to search in " + field + ": ");
        String keyword = scanner.nextLine();

        List<Furnishings> results;
        switch (field) {
            case "collection" -> results = searchSystem.searchByCollectionKeyword(keyword);
            case "name" -> results = searchSystem.searchByNameKeyword(keyword);
            case "description" -> results = searchSystem.searchByDescriptionKeyword(keyword);
            default -> {
                System.out.println("Invalid field.");
                return;
            }
        }

        displaySearchResults(results);
    }

    private void searchByPrice(Scanner scanner) {
        System.out.print("Enter the min price: ");
        double minPrice = scanner.nextDouble();

        System.out.print("Enter the max price: ");
        double maxPrice = scanner.nextDouble();

        List<Furnishings> results = searchSystem.searchByPriceRange(minPrice, maxPrice);
        displaySearchResults(results);
    }

    private void searchByColor(Scanner scanner) {
        System.out.print("Enter the furniture color: ");
        String color = scanner.nextLine();

        List<Furnishings> results = searchSystem.searchByColor(color);
        displaySearchResults(results);
    }

    private void searchByMaterial(Scanner scanner) {
        System.out.print("Enter the furniture material: ");
        String material = scanner.nextLine();

        List<Furnishings> results = searchSystem.searchByMaterial(material);
        displaySearchResults(results);
    }

    private void searchByCriteria(Scanner scanner) {
        System.out.print("Enter the furniture collection keyword (or leave empty to skip): ");
        String collectionKeyword = scanner.nextLine().toLowerCase();

        System.out.print("Enter the furniture name keyword (or leave empty to skip): ");
        String nameKeyword = scanner.nextLine().toLowerCase();

        System.out.print("Enter the min price (or leave empty to skip): ");
        String minPriceStr = scanner.nextLine();
        double minPrice = minPriceStr.isEmpty() ? Double.MIN_VALUE : Double.parseDouble(minPriceStr);

        System.out.print("Enter the max price (or leave empty to skip): ");
        String maxPriceStr = scanner.nextLine();
        double maxPrice = maxPriceStr.isEmpty() ? Double.MAX_VALUE : Double.parseDouble(maxPriceStr);

        System.out.print("Enter the furniture color (or leave empty to skip): ");
        String color = scanner.nextLine().toLowerCase();

        System.out.print("Enter the furniture material (or leave empty to skip): ");
        String material = scanner.nextLine().toLowerCase();

        SearchCriteria criteria = new SearchCriteria(collectionKeyword, nameKeyword, minPrice, maxPrice, color, material);
        List<Furnishings> results = searchSystem.searchByCriteria(criteria);
        displaySearchResults(results);
    }

    private void addUserItem(Scanner scanner) {
        try {
            System.out.println("Enter new item data:");
            System.out.println("Choose collection (1-5): ");
            System.out.println("1. contemporary");
            System.out.println("2. stylish");
            System.out.println("3. modernist");
            System.out.println("4. rural");
            System.out.println("5. outdated");
            System.out.print("Enter the collection number: ");
            int collectionNumber = scanner.nextInt();
            scanner.nextLine();

            System.out.print("Name: ");
            String name = scanner.nextLine();

            System.out.print("Description: ");
            String description = scanner.nextLine();

            System.out.print("Price: ");
            double price = scanner.nextDouble();
            scanner.nextLine();

            System.out.print("Color: ");
            String color = scanner.nextLine();

            System.out.print("Material: ");
            String material = scanner.nextLine();

            SpecFurniture newItem = new SpecFurniture(0, getCollectionName(collectionNumber), name, description, price, color, material);

            database.addRecord(newItem);

            System.out.println("New item was added successfully.");
        } catch (Exception e) {
            System.out.println("Error adding new item: " + e.getMessage());
        }
    }

    private String getCollectionName(int collectionNumber) {
        return switch (collectionNumber) {
            case 1 -> "contemporary";
            case 2 -> "stylish";
            case 3 -> "modernist";
            case 4 -> "rural";
            case 5 -> "outdated";
            default -> throw new IllegalArgumentException("Invalid collection number");
        };
    }

    private void deleteUserItems(Scanner scanner) {
        try {
            System.out.print("Enter the ID(s) of the item(s) to delete (may be separated by commas, semicolons, or spaces): ");
            String input = scanner.nextLine();

            String[] idStrings = input.split("[,;\\s]+");
            List<Integer> itemIds = Arrays.stream(idStrings)
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());

            database.deleteRecords(itemIds);

            System.out.println("Item(s) deleted successfully.");
        } catch (Exception e) {
            System.out.println("Error deleting item(s): " + e.getMessage());
        }
    }

    private void addUserCollection(Scanner scanner) {
        try {
            System.out.print("Create new collection name by simply typing it: ");
            String newCollection = scanner.nextLine();

            if (database.collectionExistsIgnoreCase(newCollection)) {
                System.out.println("Error: This category already exists. Please enter a new category.");
                addUserCollection(scanner);
            } else {
                database.addType();

                System.out.println("New category was created successfully!");

                System.out.print("Do you want to add a new record for this category? (y/n): ");
                String addRecordChoice = scanner.nextLine().toLowerCase();

                if ("y".equals(addRecordChoice)) {
                    addUserItemForCollection(scanner, newCollection);
                }
            }
        } catch (Exception e) {
            System.out.println("Error adding new category: " + e.getMessage());
        }
    }

    private void addUserItemForCollection(Scanner scanner, String collection) {
        try {
            System.out.println("Enter new item data for category '" + collection + "':");

            System.out.print("Name: ");
            String name = scanner.nextLine();

            System.out.print("Description: ");
            String description = scanner.nextLine();

            System.out.print("Price: ");
            double price = scanner.nextDouble();
            scanner.nextLine();

            System.out.print("Color: ");
            String color = scanner.nextLine();

            System.out.print("Material: ");
            String material = scanner.nextLine();

            SpecFurniture newItem = new SpecFurniture(0, collection, name, description, price, color, material);

            database.addRecord(newItem);

            System.out.println("New item added successfully.");
        } catch (Exception e) {
            System.out.println("Error adding new item: " + e.getMessage());
        }
    }

    private void displaySearchResults(List<Furnishings> results) {
        if (results.isEmpty()) {
            System.out.println("No matching furniture found.");
        } else {
            System.out.println("Matching furniture:");
            for (Furnishings furnishings : results) {
                System.out.println(furnishings);
            }
        }
    }

    private void refreshSearchSystemData() {
        List<Furnishings> furnitureList = database.loadFurnitureDataFromDatabase();
        searchSystem = new SearchSystemFurniture<>(furnitureList);
    }
}