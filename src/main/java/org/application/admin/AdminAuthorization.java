package org.application.admin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AdminAuthorization {
    private final String jdbcUrl;
    private final String dbUsername;
    private final String dbPassword;

    public AdminAuthorization() {
        this.jdbcUrl = "jdbc:postgresql://localhost:5432/Warehouse";
        this.dbUsername = "postgres";
        this.dbPassword = "200304";
    }

    public boolean isAdminAuthorized(String username, String password) {
        String query = "SELECT * FROM admin WHERE username = ? AND password = ?";
        try (Connection connection = DriverManager.getConnection(jdbcUrl, dbUsername, dbPassword);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return resultSet.next();
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}