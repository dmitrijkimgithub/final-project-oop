package org.application.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DBConnectionManager {
    private static final HikariDataSource sourceData;

    static {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/Warehouse");
        config.setUsername("postgres");
        config.setPassword("200304");
        config.setMaximumPoolSize(10);

        sourceData = new HikariDataSource(config);
    }

    public static HikariDataSource getDataSource() {
        return sourceData;
    }
}
