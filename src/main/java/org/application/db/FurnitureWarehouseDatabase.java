package org.application.db;

import com.zaxxer.hikari.HikariDataSource;
import org.application.elements.Furnishings;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FurnitureWarehouseDatabase {
    private final HikariDataSource sourceData;
    public FurnitureWarehouseDatabase(HikariDataSource dataSource) {
        this.sourceData = dataSource;
    }

    public List<Furnishings> loadFurnitureDataFromDatabase() {
        List<Furnishings> furnitureList = new ArrayList<>();

        try (Connection connection = sourceData.getConnection()) {
            String sql = "SELECT * FROM public.warehouse_table";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql);
                 ResultSet resultSet = preparedStatement.executeQuery()) {

                while (resultSet.next()) {
                    int ID = resultSet.getInt("ID");
                    String Collection = resultSet.getString("Collection");
                    String Name = resultSet.getString("Name");
                    String Description = resultSet.getString("Description");
                    double Price = resultSet.getDouble("Price");
                    String Color = resultSet.getString("Color");
                    String Material = resultSet.getString("Material");

                    Furnishings furniture = new Furnishings(ID, Collection, Name, Description, Price, Color, Material) {
                        @Override
                        public Furnishings addData(String[] data) {
                            return null;
                        }
                    };
                    furnitureList.add(furniture);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return furnitureList;
    }

    public void addRecord(Furnishings furniture) {
        try (Connection connection = sourceData.getConnection()) {
            String sql = "INSERT INTO public.warehouse_table (id, collection, name, description, price, color, material) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)";

            int newId = generateNewId(connection);

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, newId);
                preparedStatement.setString(2, furniture.getCollection());
                preparedStatement.setString(3, furniture.getName());
                preparedStatement.setString(4, furniture.getDescription());
                preparedStatement.setDouble(5, furniture.getPrice());
                preparedStatement.setString(6, furniture.getColor());
                preparedStatement.setString(7, furniture.getMaterial());

                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private int generateNewId(Connection connection) throws SQLException {
        String sql = "SELECT MAX(id) FROM public.warehouse_table";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            if (resultSet.next()) {
                int maxId = resultSet.getInt(1);
                return maxId + 1;
            } else {
                return 1;
            }
        }
    }

    public void deleteRecords(List<Integer> itemIds) {
        try (Connection connection = sourceData.getConnection()) {
            String sql = "DELETE FROM public.warehouse_table WHERE id IN (" +
                    itemIds.stream().map(id -> "?").collect(Collectors.joining(",")) + ")";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                int parameterIndex = 1;
                for (int itemId : itemIds) {
                    preparedStatement.setInt(parameterIndex++, itemId);
                }
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean collectionExistsIgnoreCase(String collection) {
        try (Connection connection = sourceData.getConnection()) {
            String sql = "SELECT COUNT(*) FROM public.warehouse_table WHERE LOWER(collection) = LOWER(?)";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, collection);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        int count = resultSet.getInt(1);
                        return count > 0;
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public void addType() {
    }
}