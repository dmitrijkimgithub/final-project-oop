package org.application.elements;

public abstract class Furnishings {

    public int id;
    public String collection;
    public String name;
    public String description;
    public double price;
    public String color;
    public String material;

    public Furnishings(int id, String collection, String name, String description, double price, String color,
                       String material) {
        this.id = id;
        this.collection = collection;
        this.name = name;
        this.description = description;
        this.price = price;
        this.color = color;
        this.material = material;
    }

    public int getId() {
        return id;
    }

    public String getCollection() {
        return collection;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public String getColor() {
        return color;
    }

    public String getMaterial() {
        return material;
    }
    @Override
    public String toString() {
        return "Furniture [ID: " + id + ", Collection: " + collection + ", Name: " + name + ", Description: " + description + ", Price: " + price + ", Color: " + color + ", Material: " + material + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Furnishings furniture = (Furnishings) obj;
        return id == furniture.id;
    }

    public Furnishings()
    {
    }

    public abstract Furnishings addData(String[] data);

    @Override
    public int hashCode() {
        return Integer.hashCode(id);
    }
}