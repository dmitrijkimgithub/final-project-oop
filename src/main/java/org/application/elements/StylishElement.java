package org.application.elements;

public class StylishElement extends Furnishings {

    public StylishElement() {
    }

    @Override
    public StylishElement addData(String[] data) {

        this.id = Integer.parseInt(data[0]);
        this.collection = data[1];
        this.name = data[2];
        this.description = data[3];
        this.price = Double.parseDouble(data[4]);
        this.color = data[5];
        this.material = data[6];

        return this;
    }
}
