package org.application.elements;

public class SpecFurniture extends Furnishings {
    public SpecFurniture(int id, String collection, String name, String description, double price, String color, String material) {
        super(id, collection, name, description, price, color, material);
    }

    @Override
    public Furnishings addData(String[] data) {
        int id = Integer.parseInt(data[0]);
        String collection = data[1];
        String name = data[2];
        String description = data[3];
        double price = Double.parseDouble(data[4]);
        String color = data[5];
        String material = data[6];
        return new SpecFurniture(id, collection, name, description, price, color, material);
    }
}

