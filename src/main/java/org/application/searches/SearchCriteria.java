package org.application.searches;

import org.application.interfaces.SearchCriteriaInterface;
import org.application.elements.Furnishings;

public record SearchCriteria<T extends Furnishings>(String collection, String name, double minPrice, double maxPrice, String color, String material) implements SearchCriteriaInterface<T> {
}