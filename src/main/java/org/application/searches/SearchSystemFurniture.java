package org.application.searches;

import org.application.interfaces.SearchSystemInterface;
import org.application.elements.Furnishings;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SearchSystemFurniture<T extends Furnishings> implements SearchSystemInterface<T> {

    private final List<T> furnitureList;

    public SearchSystemFurniture(List<T> furnitureList) {
        this.furnitureList = furnitureList;
    }

    public List<T> searchById(int id) {
        return filterFurniture(f -> f.id == id);
    }

    public List<T> searchByCollectionKeyword(String keyword) {
        return filterFurniture(f -> f.collection.toLowerCase().contains(keyword.toLowerCase()));
    }

    public List<T> searchByNameKeyword(String keyword) {
        return filterFurniture(f -> f.name.toLowerCase().contains(keyword.toLowerCase()));
    }

    public List<T> searchByDescriptionKeyword(String keyword) {
        return filterFurniture(f -> f.description.toLowerCase().contains(keyword.toLowerCase()));
    }

    public List<T> searchByPriceRange(double minPrice, double maxPrice) {
        return filterFurniture(f -> f.price >= minPrice && f.price <= maxPrice);
    }

    public List<T> searchByColor(String color) {
        return filterFurniture(f -> f.color.equalsIgnoreCase(color));
    }

    public List<T> searchByMaterial(String material) {
        return filterFurniture(f -> f.material.equalsIgnoreCase(material));
    }

    public List<T> searchByCriteria(SearchCriteria<T> criteria) {
        Predicate<T> predicate = f ->
                (criteria.name() == null || criteria.name().isEmpty() || f.name.toLowerCase().contains(criteria.name().toLowerCase()))
                        && (f.price >= criteria.minPrice() && f.price <= criteria.maxPrice())
                        && (criteria.color() == null || criteria.color().isEmpty() || f.color.equalsIgnoreCase(criteria.color()))
                        && (criteria.material() == null || criteria.material().isEmpty() || f.material.equalsIgnoreCase(criteria.material()))
                        && (criteria.collection() == null || criteria.collection().isEmpty() || f.collection.toLowerCase().contains(criteria.collection().toLowerCase()));

        return filterFurniture(predicate);
    }

    private List<T> filterFurniture(Predicate<T> predicate) {
        return furnitureList.stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }
}
