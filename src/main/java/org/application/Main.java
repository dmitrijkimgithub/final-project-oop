package org.application;

import com.zaxxer.hikari.HikariDataSource;
import org.application.admin.AdminAuthorization;
import org.application.controlpanel.ControlPanel;
import org.application.db.DBConnectionManager;
import org.application.db.FurnitureWarehouseDatabase;
import org.application.searches.SearchSystemFurniture;
import org.application.elements.Furnishings;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        HikariDataSource dataSource = DBConnectionManager.getDataSource();

        FurnitureWarehouseDatabase furnitureDatabase = new FurnitureWarehouseDatabase(dataSource);

        List<Furnishings> furnitureList = furnitureDatabase.loadFurnitureDataFromDatabase();

        SearchSystemFurniture<Furnishings> searchEngine = new SearchSystemFurniture<>(furnitureList);

        AdminAuthorization authorizationService = new AdminAuthorization();

        ControlPanel console = new ControlPanel(searchEngine, authorizationService, furnitureDatabase);

        console.start();

        dataSource.close();
    }
}

