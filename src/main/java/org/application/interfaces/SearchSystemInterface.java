package org.application.interfaces;

import org.application.searches.SearchCriteria;
import org.application.elements.Furnishings;

import java.util.List;

public interface SearchSystemInterface<T extends Furnishings> {
    List<T> searchById(int id);
    List<T> searchByCollectionKeyword(String keyword);
    List<T> searchByNameKeyword(String keyword);
    List<T> searchByPriceRange(double minPrice, double maxPrice);
    List<T> searchByColor(String color);
    List<T> searchByMaterial(String material);
    List<T> searchByCriteria(SearchCriteria<T> criteria);
}


