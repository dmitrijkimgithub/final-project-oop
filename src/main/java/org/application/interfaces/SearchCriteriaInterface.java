package org.application.interfaces;

import org.application.elements.Furnishings;

public interface SearchCriteriaInterface<T extends Furnishings> {
    String name();
    double minPrice();
    double maxPrice();
    String color();
    String material();
}
