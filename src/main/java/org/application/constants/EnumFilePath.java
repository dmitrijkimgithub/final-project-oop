package org.application.constants;

import org.application.elements.*;

public enum EnumFilePath {
    contemporary, stylish, modernist, rural, outdated;

    public String getPath() {
        return "src/main/resources/" + this.name() + ".csv";
    }

    public Furnishings getElement() {
        return switch (this.name()) {
            case "contemporary":
                yield new ContemporaryElement();
            case "stylish":
                yield new StylishElement();
            case "modernist":
                yield new ModernistElement();
            case "rural":
                yield new RuralElement();
            case "outdated":
                yield new OutdatedElement();
            default:
                yield null;
        };
    }
}
