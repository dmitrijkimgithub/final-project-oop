create table admin (
user_id SERIAL PRIMARY key,
username varchar(255),
password varchar(255)
);

insert into "admin" (username, password)
values ('itpuPostgres', 'student' );

create table warehouse_table ( 
ID SERIAL primary key,
Collection text, 
Name varchar(255),
Description text,
Price DECIMAL(10,2),
Color text,
Material text
);

COPY warehouse_table(ID, Collection, Name, Description, Price, Color, Material)
FROM 'C:\final-project-oop\src\main\resources\all_in_one_table.csv'
DELIMITER ','
CSV HEADER;